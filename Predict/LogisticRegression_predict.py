from Predict import Predict
import numpy as np
from sklearn.metrics import accuracy_score

path = input('Enter LR model 11 or 13: ')
model = Predict(path)

csv_path = str(input("Enter csv file: "))

model.load_model()

model.load_input_from_csv(csv_path)

print('Predicted labels:\n' + str(model.predict()))
print('Predict probability: \n' + str(model.predict_proba()))

