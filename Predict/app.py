from flask import Flask, request, jsonify, render_template
from sklearn.externals import joblib
from Predict import Predict
import traceback
import pandas as pd
import numpy as np
import json

app = Flask(__name__)
MODEL = Predict('MLP_8.joblib')
MODEL.load_model()
MODEL_LABELS = ['1', '0']

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/predict',methods=['POST'])
def predict():
    if MODEL:
        try:
            json_ = request.json
            title = (str(request.form.get('title')))
            # titlecompare = (request.form.getlist('titlecompare')[0].split(';'))
            titlecompares = json.loads(request.form.get('titlecompare'))
            features = np.array([title, titlecompares])
            MODEL.load_input_from_json(title, titlecompares)
            label_index = np.array(MODEL.predict())
            percent= np.max(MODEL.predict_proba(), axis = 1)
            result = ""
            for i in range(label_index.shape[0]):  
                result += title + " - " + titlecompares[i] + ': ' + str(label_index[i]) + ',Percent:' + str(float(percent[i])) +'\n'
            return render_template('index.html', prediction_text='Result: {}\n'.format({result}))
        except:
            return jsonify({'trace': traceback.format_exc()})
    else:
        return
if __name__ == "__main__":
    app.run(debug=True)

